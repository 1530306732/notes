# 全局变量至汇编的三种不同mov指令
x86的mov指令可以在imm(立即数），内存地址和寄存器直接移动数据（当然imm不能作为移动的dest）。
但mov指令不能在内存和内存之间直接移动。内存和内存直接的移动可以用movs（字符串移动）指令完成。

假设定义有如下的一个全局变量`int g_val=11`,
```assembly
.data
.global g_val
.type g_val,@object
g_val:
  .long 11
.size g_val,.-g_val
```

下面第一种mov指令使用方式对应gcc编译不开启fPIE/fPIC参数编译结果，第二种mov指令使用方式，对应gcc的fPIE/fPIC参数开启的编译结果。fPIC/fPIE作用于compile阶段，汇编和链接阶段就不起作用了。
```
mov g_val,%eax 
movl g_val(%rip),%eax 
mov $g_val,%eax
```
第三种使用方式, 表示将g_val的地址作为立即数存入edi寄存器，起效果等效于`mov $0x10,%eax`,不多对于链接器来说，前者需要利`g_val`符号的值完成一个重定位。

更多的一些例子，
```assembly
mov (%ebx), %eax //从ebx寄存器存放地址取4B到eax
mov %ebx, var(,1)  //将ebx中的内存存入var变量????
mov -4(%esi), %eax  /* Move 4 bytes at memory address ESI + (-4) into EAX. */
mov %cl, (%esi,%eax,1)      /* Move the contents of CL into the byte at address ESI+EAX. */
mov (%esi,%ebx,4), %edx         /* Move the 4 bytes of data at address ESI+4*EBX into EDX. */
```

## Something more

1. 在x86汇编最后建议以newline结束。也就是最后加一个空行。

2. `git checkout --orphan <newBranch>`创建一个新的branch。

3. 所以源码中的变量，最后都会转化为内存地址（运行时可以看成是常量数）。在汇编中对应到各种labels，汇编中labels或数字，作为立即数使用时，经常在前面加一个`$`符号。若不加`$`则表示内存地址，反应在mov指令中的区别就是：
`mov 0x1000, %rax`表示从0x1000地址处的内容存入rax寄存器。`mov $0x1000, %rax`表示将0x1000这个常数存入rax寄存器。

4. `mov 内存地址，寄存器`是取内存地址中对应的数据到寄存器，而要将内存地址本身存入寄存器怎么办？用`leaq 内存地址，寄存器`。`leaq`指令取地址就也有类似三种不同的表达，
```assembly
leaq   g_val1(%rip), %rax //相对方式去变量g_val1的地址，开fPIE/fPIC时用,
                    // 48 8d 3d 00 00 00 00 -> 对应的二进制指令  R_X86_64_PC32重定位
leaq   g_val1, %rax //绝对方式去变量g_val1的地址，这个从某种意义上说和`mov $g_val1,%rax`指令是等价的。 
                    // 8:   48 8d 3c 25 00 00 00   对应的二进制指令，R_X86_64_32S ->用到的重定位信息
leaq  $g_val1, %rax //错误的指令，lea的src只能是内存地址，不能是立即数。
```

5. wsl中默认的gcc编译参数都是带fpie,链接阶段-pie参数。所以编译的时候，对数据的访问都是采用相对的方式。所以写汇编时上面的第一种方式是不行的，不能用于和main.o进行默认链接，可通过添加-static参数完成链接，但这种方式编译出来的bin会比较大，还可以先编译时`-v`参数，显示最后的链接命令，再去掉其中的`-pie`参数，手动编一下
```
app : fun.s main.c
    gcc fun.s -c -fno-PIE 
    gcc -c main.c -fno-PIE 
    #gcc main.o fun.o -o app -v
    /usr/lib/gcc/x86_64-linux-gnu/9/collect2 -plugin /usr/lib/gcc/x86_64-linux-gnu/9/liblto_plugin.so -plugin-opt=/usr/lib/gcc/x86_64-linux-gnu/9/lto-wrapper -plugin-opt=-fresolution=/tmp/ccEDAy9P.res -plugin-opt=-pass-through=-lgcc -plugin-opt=-pass-through=-lgcc_s -plugin-opt=-pass-through=-lc -plugin-opt=-pass-through=-lgcc -plugin-opt=-pass-through=-lgcc_s --build-id --eh-frame-hdr -m elf_x86_64 --hash-style=gnu --as-needed -dynamic-linker /lib64/ld-linux-x86-64.so.2 -z now -z relro -o app /usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu/Scrt1.o /usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu/crti.o /usr/lib/gcc/x86_64-linux-gnu/9/crtbeginS.o -L/usr/lib/gcc/x86_64-linux-gnu/9 -L/usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu -L/usr/lib/gcc/x86_64-linux-gnu/9/../../../../lib -L/lib/x86_64-linux-gnu -L/lib/../lib -L/usr/lib/x86_64-linux-gnu -L/usr/lib/../lib -L/usr/lib/gcc/x86_64-linux-gnu/9/../../.. main.o fun.o -lgcc --push-state --as-needed -lgcc_s --pop-state -lc -lgcc --push-state --as-needed -lgcc_s --pop-state /usr/lib/gcc/x86_64-linux-gnu/9/crtendS.o /usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu/crtn.o
```

6. 虽然imm->寄存器/内存地址，内存地址->寄存器，寄存器->内存地址，寄存器->寄存器的字面指令都是mov,但对应二进制的opcode并不同。也就是汇编器需要通过种种细节识别出mov字符指令到底对应底层什么opcode。